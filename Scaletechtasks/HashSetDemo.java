import java.util.*;

class HashSetDemo
{
	public static void main(String args[])
	{
		long starting=System.currentTimeMillis();
//Inserting values
		HashSet<Integer> hashint=new HashSet<Integer>();
		for(int i=1;i<=10;i++)
		{
			hashint.add(i);
		}
//Updating a value
		int value,newvalue;
		Scanner sc=new Scanner(System.in);
		/*System.out.println("Enter value to be updated?");
		value=sc.nextInt();
		System.out.println("Enter new value ?");
		newvalue=sc.nextInt();
		if(hashint.contains(value))
		{
			hashint.remove(value);
			hashint.add(newvalue);
		}*/
		
//Deleting a value
		System.out.println("Enter value to be deleted?");
		
		value=sc.nextInt();
		hashint.remove(value);
		
//Printing values
		Iterator<Integer> itr=hashint.iterator();
		System.out.println("Current Set!!!");
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		long ending=System.currentTimeMillis();
		System.out.println("Execution Time = " + (ending-starting) + " ms");
	}
}